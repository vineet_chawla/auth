var express = require('express');
var router = express.Router();
var User = require('../models/user');
var path = require('path');
let currentuser;


// GET route for reading data
router.get('/', function (req, res, next) {
  return res.sendFile(path.join(__dirname + '/templateLogReg/index.html'));
});

router.get('/game', function (req, res, next) {
  currentuser = {
    username: req.session.userName,
    asset: req.session.asset
  };
  return res.sendFile(path.resolve(__dirname + './../templateLogReg/game.html'));
});

//POST route for updating data
router.post('/', function (req, res, next) {
  // confirm that user typed same password twice
  if (req.body.password !== req.body.passwordConf) {
    var err = new Error('Passwords do not match.');
    err.status = 400;
    res.send("passwords dont match");
    return next(err);
  }

  if (req.body.email &&
    req.body.username &&
    req.body.password &&
    req.body.passwordConf) {

    var userData = {
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
    }

    User.create(userData, function (error, user) {
      if (error) {
        return next(error);
      } else {
        req.session.userId = user._id;
        req.session.userName = user.username;
        req.session.asset = user.asset;
        return res.redirect('/game');
      }
    });

  } else if (req.body.logemail && req.body.logpassword) {
    User.authenticate(req.body.logemail, req.body.logpassword, function (error, user) {
      if (error || !user) {
        var err = new Error('Wrong email or password.');
        err.status = 401;
        return next(err);
      } else {
        req.session.userId = user._id;
        req.session.userName = user.username;
        req.session.asset = user.asset;
        return res.redirect('/game');
      }
    });
  } else {
    var err = new Error('All fields required.');
    err.status = 400;
    return next(err);
  }
})

router.post('/changeOwnership', (req, res, next) => {
  if((req.body.username || req.body.assetId) == '') {
    console.log("Values missing");
    res.status(404).send("Missing values in post request");
  }
  let asset = req.body.assetId;
  let filter = {username: req.body.username};
  let update = { asset : asset};
  User.findOneAndUpdate(filter, update, (err, data) => {
    if (err) 
      res.send(err);
    res.send(data)
  } );
})

// GET route after registering
router.get('/profile', function (req, res, next) {
  User.findById(req.session.userId)
    .exec(function (error, user) {
      if (error) {
        return next(error);
      } else {
        if (user === null) {
          var err = new Error('Not authorized! Go back!');
          err.status = 400;
          return next(err);
        } else {
          return res.send('<h1>Name: </h1>' + user.username + '<h2>Mail: </h2>' + user.email + '<br><a type="button" href="/logout">Logout</a>')
        }
      }
    });
});

// GET route after registering
router.get('/currentuser', function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  return res.send({"currentuser": currentuser});
});

// GET for logout logout
router.get('/logout', function (req, res, next) {
  if (req.session) {
    // delete session object
    req.session.destroy(function (err) {
      if (err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }
});

module.exports = router;