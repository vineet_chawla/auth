var config = {
    type: Phaser.AUTO,
    parent: 'phaser-example',
    width: 900,
    height: 800,
    backgroundColor: '#267352',
    physics: {
      default: 'arcade',
      arcade: {
        debug: false,
        gravity: { y: 0 }
      }
    },
    scene: {
      preload: preload,
      create: create,
      update: update
    } 
  };
var game = new Phaser.Game(config);
// import {cfg} from './../../config'
// // var cfg = require('./../../config')
// console.log(cfg.dbIp)

  

  const createText = (game = game, target, playerName) =>

    game.add.text(target.x, target.y, playerName, {
      fontSize: '12px',
      fill: '#FFF',
      align: 'center'
  })
   
  function preload() {
    let ship;
    getUserData()
    .then(user => {
      ship = user.asset;
      game.username = user.username;
      return Promise.resolve(ship);
    })
    .then(ship => {
      game.scene.scenes[0].load.image(`ship`, `assets/${ship}.png`);
      // game.load.image('otherPlayer', 'assets/enemyBlack5.png');
    })
    // this.load.image(`ship`, `assets/spaceShips_002.png`);
      this.load.image('otherPlayer', 'assets/racecar1.png');
      this.load.image('star', 'assets/star_gold.png');
      this.load.image('bg', 'assets/bg.png');
  }
  
  function create() {
    var self = this;
    this.socket = io();
    this.add.image(450, 400, 'bg').setDisplaySize(900, 800);
    this.otherPlayers = this.physics.add.group();
    this.openingText = this.add.text(
      this.physics.world.bounds.width / 2,
      this.physics.world.bounds.height / 2,
      'First player to score 100 wins',
      {
        fontFamily: 'Monaco, Courier, monospace',
        fontSize: '50px',
        fill: '#fff'
      }
    );
    this.blueWonText = this.add.text(
      this.physics.world.bounds.width / 2,
      this.physics.world.bounds.height / 2,
      'The blue team won',
      {
        fontFamily: 'Monaco, Courier, monospace',
        fontSize: '50px',
        fill: '#fff',
        visible: false
      }
    );
    
    this.redWonText = this.add.text(
      this.physics.world.bounds.width / 2,
      this.physics.world.bounds.height / 2,
      'The red team won',
      {
        fontFamily: 'Monaco, Courier, monospace',
        fontSize: '50px',
        fill: '#fff',
        visible: false
      }
    );
    
    this.openingText.setOrigin(0.5);
    this.redWonText.setOrigin(0.5).setVisible(false);
    this.blueWonText.setOrigin(0.5).setVisible(false);
    
    this.socket.on('currentPlayers', players => {
      Object.keys(players).forEach(id => {
        if (players[id].playerId === self.socket.id) {
          addPlayer(self, players[id]);
        } else {
          addOtherPlayers(self, players[id]);
        }
      });
    });
    this.socket.on('newPlayer', function (playerInfo) {
      self.redWonText.setOrigin(0.5).setVisible(false);
      self.blueWonText.setOrigin(0.5).setVisible(false);
      addOtherPlayers(self, playerInfo);
    });
    this.socket.on('disconnect', function (playerId) {
      self.otherPlayers.getChildren().forEach(function (otherPlayer) {
        if (playerId === otherPlayer.playerId) {
          otherPlayer.destroy();
        }
      });
    });
    this.cursors = this.input.keyboard.createCursorKeys();

    this.socket.on('playerMoved', function (playerInfo) {
      self.otherPlayers.getChildren().forEach(function (otherPlayer) {
        if (playerInfo.playerId === otherPlayer.playerId) {
          otherPlayer.setRotation(playerInfo.rotation);
          otherPlayer.setPosition(playerInfo.x, playerInfo.y);
          if(otherPlayer.otherName) {
            otherPlayer.otherName.x = playerInfo.x;
            otherPlayer.otherName.y = playerInfo.y;
          } else {
            otherPlayer.otherName = createText(self, otherPlayer, playerInfo.playerName);
          }
        }
      });
    });
    this.blueScoreText = this.add.text(16, 16, '', { fontSize: '32px', fill: '#0000FF' });
    this.redScoreText = this.add.text(584, 16, '', { fontSize: '32px', fill: '#FF0000' });
      
    this.socket.on('scoreUpdate', scores => {
      self.blueScoreText.setText('Team Blue: ' + scores.blue);
      self.redScoreText.setText('Team Red: ' + scores.red);
      if(scores.blue > 90) {
        self.otherPlayers.destroy(true);
        this.blueWonText.setVisible(true);
      } else if(scores.red > 90) {

        self.otherPlayers.destroy(true);
        this.redWonText.setVisible(true);
      }
    });

    this.socket.on('starLocation', starLocation => {
      console.log("consuming star location");
      console.log("new star image at ", starLocation.x, starLocation)
      if (self.star) self.star.destroy();
      self.star = self.physics.add.image(starLocation.x, starLocation.y, 'star');
      self.physics.add.overlap(self.ship, self.star, () => {
        this.socket.emit('starCollected', self.star);
      }, null, self);
    })
  }
   
  function update() {
    var self = this;
    if (this.ship) {
      if (this.cursors.left.isDown) {
        this.ship.setAngularVelocity(-300);
      } else if (this.cursors.right.isDown) {
        this.ship.setAngularVelocity(300);
      } else {
        this.ship.setAngularVelocity(0);
      }

    
      if (this.cursors.up.isDown) {
        this.openingText.setVisible(false);
        this.physics.velocityFromRotation(this.ship.rotation + 1.5, 100, this.ship.body.acceleration);
      } else {
        this.ship.setAcceleration(0);
      }
      this.physics.world.wrap(this.ship, 5);
      // emit player movement
      var x = this.ship.x;
      var y = this.ship.y;
      var r = this.ship.rotation;
      if (this.ship.oldPosition && (x !== this.ship.oldPosition.x || y !== this.ship.oldPosition.y || r !== this.ship.oldPosition.rotation)) {
        this.ship.playerName.x = this.ship.x;
        this.ship.playerName.y = this.ship.y;
        this.socket.emit('playerMovement', { x: this.ship.x, y: this.ship.y, rotation: this.ship.rotation, playerName: this.ship.playerName.text });
      }

      // save old position data
      this.ship.oldPosition = {
        x: this.ship.x,
        y: this.ship.y,
        playerName: this.ship.playerName,
        rotation: this.ship.rotation
      };
    }
  };

  function randomPosition(max) {
    return Math.floor(Math.random() * max) + 50;
  }

  function addPlayer(self, playerInfo) {
    self.ship = self.physics.add.image(playerInfo.x, playerInfo.y, 'ship').setOrigin(0.5, 0.5).setDisplaySize(50, 80).setBounce(0.5,0.5);
    self.text = self.add.text(self.ship.x, self.ship.y, playerInfo.playerName);
    // self.physics.add.collider(self.ship, self.otherPlayers);
    if (playerInfo.team === 'blue') {
      self.ship.setTint(0x0000ff);
    } else {
      self.ship.setTint(0xff0000);
    }
    self.ship.setDrag(100);
    self.ship.setAngularDrag(100);
    self.ship.setMaxVelocity(200);
    self.ship.playerName = createText(self, self.ship, game.username);
  }

  function addOtherPlayers (self, playerInfo) {
    const otherPlayer = self.add.sprite(playerInfo.x, playerInfo.y, 'otherPlayer').setOrigin(0.5, 0.5).setDisplaySize(50, 80);
    if (playerInfo.team === 'blue') {
      otherPlayer.setTint(0x000066);
    } else {
      otherPlayer.setTint(0x660000);
    }
    otherPlayer.playerId = playerInfo.playerId;
    self.otherPlayers.add(otherPlayer);
    self.otherPlayers.otherName = createText(self, otherPlayer, playerInfo.playerName);
  }

  async function getUserData () {
    // either a request or function call
    var url = "/currentuser";
    let user;
    await fetch(url)
    .then(response => {
      return response.json();
    })
    .then(data=>{
      user = data.currentuser;
    }).catch(err=> {
      console.log(`faced error getting user data ${err}`);
      return Promise.reject(err);
    })
    return Promise.resolve(user);
  }