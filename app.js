var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var server = require('http').Server(app);
var io = require('socket.io').listen(server);
const cors = require("cors");
const cfg = require('./config')
require('dotenv').config()

var players = {};
var oldStarLocation = {};
var star = {
  x : Math.floor(Math.random() * 700) + 50,
  y : Math.floor(Math.random() * 700) + 50
};
var scores = {
  blue: 0,
  red:0
}
var teamColourFlag = 1;

//connect to MongoDB
mongoose.connect(process.env.MONGODB_URI || `mongodb://${cfg.dbIp}/testForAuth`);
var db = mongoose.connection;


//handle mongo error
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  // we're connected!
});

//use sessions for tracking logins
app.use(session({
  secret: 'work hard',
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: db
  })
}));

// parse incoming requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


// serve static files from template
app.use(express.static(__dirname + '/templateLogReg'));

// include routes
var routes = require('./routes/router');
app.use('/', routes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('File Not Found');
  err.status = 404;
  next(err);
});

app.use(cors());

// error handler
// define as the last app.use callback
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.message);
});

io.on('connection', function (socket) {
  let team;
  console.log('a user connected');
  console.log( 'Resetting Scores');
  scores.red = scores.blue = 0;
  if(teamColourFlag % 2 == 0) {
    team = 'red';
    teamColourFlag++;
  } else {
    team = 'blue'
    teamColourFlag++;

  }
  // create a new player and add it to our players object
  players[socket.id] = {
    rotation: 0,
    x: Math.floor(Math.random() * 700) + 50,
    y: Math.floor(Math.random() * 500) + 50,
    playerId: socket.id,
    playerName: '',
    team: team
  };
  // send the players object to the new player
  socket.emit('currentPlayers', players);
  // update all other players of the new player
  socket.broadcast.emit('newPlayer', players[socket.id]);
  socket.emit('starLocation', star);
  socket.emit('scoreUpdate', scores);
  socket.on('disconnect', function () {
    console.log('user disconnected');
    // remove this player from our players object
    delete players[socket.id];
    // emit a message to all players to remove this player
    io.emit('disconnect', socket.id);
  });
  // when a player moves, update the player data
  socket.on('playerMovement', movementData => {
    players[socket.id].x = movementData.x;
    players[socket.id].y = movementData.y;
    players[socket.id].rotation = movementData.rotation;
    players[socket.id].playerName.x = movementData.x;
    players[socket.id].playerName.y = movementData.y;
    players[socket.id].playerName = movementData.playerName;
    // emit a message to all players about the player that moved
    socket.broadcast.emit('playerMoved', players[socket.id]);
  });            

  socket.on('starCollected', oldStar => {
    //adding for race condition 
    console.log("oldStarLocation:", oldStarLocation)
    if(oldStarLocation && oldStarLocation.x !== oldStar.x && oldStarLocation.y !== oldStar.y ) {
      console.log("old star location", oldStar.x, oldStar.y);    
      if (players[socket.id].team === 'red'){
        console.log("red collected");
        console.log("score is" + scores.red );
        scores.red += 10;
      } else {
        console.log("red collected");
        console.log("score is" + scores.red );
        scores.blue += 10;
      }
      star.x = Math.floor(Math.random() * 700) + 50;
      star.y = Math.floor(Math.random() * 700) + 50;
      io.emit('starLocation', star);
      console.log("emmited star location");
      io.emit('scoreUpdate', scores);
      console.log("emmited scores");
      oldStarLocation.x = oldStar.x;
      oldStarLocation.y = oldStar.y;
    }
  });
});

const port = process.env.PORT || 3000;

// listen on port 3000
server.listen(port, function () {
  console.log('Express app listening on port 3000');
});