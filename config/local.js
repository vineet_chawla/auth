const cfg = {
    gameIP:  '172.25.16.95',
    gamePort: 3000,
    gameURL: 'http://172.25.16.95:3000',
    marketplaceIP:'192.168.70.128',
    marketplacePort:3000,
    dbIp: '172.25.16.95',
    dBPort: 27017
}

module.exports = cfg;